﻿from enum import Enum
import re

JSON_FILE_PATH = "json/"
HTML_FILE_PATH = "html/"
RUBY_FILE_PATH = "ruby/"

class FaqStatus(Enum):
    PUBLISHED = 0
    UNPUBLISHED = 1

faqStatusSymbols = {
    FaqStatus.PUBLISHED: ':active',
    FaqStatus.UNPUBLISHED: ':inactive'
}

def is_sub_index(index, sub_index):
    if index == sub_index:
        return False
    elif index in sub_index:
        return True
    else:
        return False

def get_super_index(index):
    if is_index(index):
        return index[:index.rfind('.')]
    else:
        return None

def is_index(index):
    if re.match(r"([1-9]+\.)+", index):
        return True
    else:
        return False

def is_sub_index(index, sub_index):
    if index == sub_index:
        return False

    return sub_index.startswith(index)

def get_level(index):
    return index.count('.')
 

def is_h_tag(tag):
    return tag.name.startswith('h') and tag.name[1:] != '1'