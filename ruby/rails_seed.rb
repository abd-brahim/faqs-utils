continente_online = Store.find_by(slug: 'continente-online')
online_note = Store.find_by(slug: 'online-note')
faq_1 = Faq.create(title: 'Registo', title_index: '1.', description: '', status: :activecontinente_online.faqs << faq_1
online_note.faqs << faq_1

faq_1_1 = Faq.create(title: 'Tenho de estar registado no site para fazer a minha encomenda?', title_index: '1.1.', description: 'Não é necessário estar registado no site para conseguir fazer a sua reserva e encomenda. Basta clicar no botão
        “Comprar Livros Escolares” e iniciar a sua encomenda!<br/><br/>', status: :active, associated_faq_id: faq_1_.id
continente_online.faqs << faq_1_1
online_note.faqs << faq_1_1

faq_2 = Faq.create(title: 'Reservas', title_index: '2.', description: '', status: :activecontinente_online.faqs << faq_2
online_note.faqs << faq_2

faq_2_1 = Faq.create(title: 'Como posso fazer a minha reserva?', title_index: '2.1.', description: 'Para iniciar uma reserva, efetue os seguintes passos:<ol class="ui list" role="list">
<li class="" role="listitem">Clique em “Comprar Livros Escolares”;</li>
<li class="" role="listitem">Preencha os campos com os seus dados, escolha os Livros que pretende reservar e
            clique em “Comprar”;</li>
<li class="" role="listitem">Selecione se pretende proteger os Livros com Encapamento e introduza o seu número
            de Cartão Continente ou Universo para usufruir de todos os descontos e vantagens;</li>
<li class="" role="listitem">Escolha o local de entrega, que poderá ser em loja ou ao domicílio. Introduza os
            seus dados, incluindo o seu contacto telefónico (obrigatório), procedendo à validação do mesmo, através de
            um código de 4 dígitos que irá receber por SMS;</li>
<li class="" role="listitem">Por fim, escolha o método de pagamento e clique em “Pagar”. A reserva será
            confirmada por SMS e email, sendo partilhado o número da sua reserva e dados de pagamento.</li>
</ol><li class="" role="listitem">Clique em “Comprar Livros Escolares”;</li><li class="" role="listitem">Preencha os campos com os seus dados, escolha os Livros que pretende reservar e
            clique em “Comprar”;</li><li class="" role="listitem">Selecione se pretende proteger os Livros com Encapamento e introduza o seu número
            de Cartão Continente ou Universo para usufruir de todos os descontos e vantagens;</li><li class="" role="listitem">Escolha o local de entrega, que poderá ser em loja ou ao domicílio. Introduza os
            seus dados, incluindo o seu contacto telefónico (obrigatório), procedendo à validação do mesmo, através de
            um código de 4 dígitos que irá receber por SMS;</li><li class="" role="listitem">Por fim, escolha o método de pagamento e clique em “Pagar”. A reserva será
            confirmada por SMS e email, sendo partilhado o número da sua reserva e dados de pagamento.</li><br/>', status: :active, associated_faq_id: faq_2_.id
continente_online.faqs << faq_2_1
online_note.faqs << faq_2_1

faq_2_10 = Faq.create(title: 'Na mesma compra posso adquirir Livros Escolares com e sem voucher MEGA?', title_index: '2.10.', description: 'Sim, na mesma compra pode adquirir Livros Escolares com e sem voucher MEGA. Para tal, aquando da seleção dos
        Livros desejados, deverá adicionar o voucher MEGA referente ao livro selecionado e esse valor será descontado no
        preço a pagar.<p>No caso da compra de Livros sem voucher MEGA, basta adicioná-los à lista e o valor do livro será acrescentado ao
        seu carrinho.</p><br/>', status: :active, associated_faq_id: faq_2_1.id
continente_online.faqs << faq_2_10
online_note.faqs << faq_2_10

faq_2_11 = Faq.create(title: 'Na mesma compra posso adquirir Livros Escolares para mais do que um aluno?', title_index: '2.11.', description: 'Sim, na mesma compra consegue adquirir Livros Escolares para vários alunos. Para tal, basta clicar em “Adicionar
        aluno”, no topo da página, preencher os dados referentes ao mesmo e selecionar os Livros Escolares desejados.
    <br/>', status: :active, associated_faq_id: faq_2_1.id
continente_online.faqs << faq_2_11
online_note.faqs << faq_2_11

faq_2_12 = Faq.create(title: 'Posso consultar a lista de Livros Escolares encomendados, por aluno?', title_index: '2.12.', description: 'Para facilitar a sua compra, disponibilizamos uma lista de livros adotados por escola / ano escolar. Para além
        disso, poderá consultar a lista de artigos encomendados, para cada aluno.<br/>', status: :active, associated_faq_id: faq_2_1.id
continente_online.faqs << faq_2_12
online_note.faqs << faq_2_12

faq_continente_online_2_13 = Faq.create(title: 'Onde posso ver o estado da minha encomenda?', title_index: '2.13.', description: 'Tem duas opções para consultar o estado da sua encomenda: Vá a <a href="https://livrosescolares.continente.pt" rel="noreferrer" target="_blank">continente.pt</a>, clique em “Gerir Reserva”, introduza o número de
        telefone e da reserva e clique em “Iniciar Sessão".<a href="https://livrosescolares.continente.pt" rel="noreferrer" target="_blank">continente.pt</a><p><strong>NOTA: </strong>Como alternativa, pode clicar no botão “Acompanhar Reserva” que está no seu email de
        confirmação da mesma. Será redirecionado para uma página onde consta toda a informação sobre a sua encomenda.
    </p><strong>NOTA: </strong><br/>', status: :active, associated_faq_id: faq_2_1.id
continente_online.faqs << faq_continente_online_2_13

faq_online_note_2_13 = Faq.create(title: 'Onde posso ver o estado da minha encomenda?', title_index: '2.13.', description: 'Tem duas opções para consultar o estado da sua encomenda: Vá a <a href="https://www.noteonline.pt" rel="noreferrer" target="_blank">noteonline.pt</a>, clique em “Gerir Reserva”, introduza o número de
        telefone e da reserva e clique em “Iniciar Sessão".<a href="https://www.noteonline.pt" rel="noreferrer" target="_blank">noteonline.pt</a><p><strong>NOTA: </strong>Como alternativa, pode clicar no botão “Acompanhar Reserva” que está no seu email de
        confirmação da mesma. Será redirecionado para uma página onde consta toda a informação sobre a sua encomenda.
    </p><strong>NOTA: </strong><br/>', status: :active, associated_faq_id: faq_2_1.id
online_note.faqs << faq_online_note_2_13

faq_2_14 = Faq.create(title: 'Posso cancelar a minha encomenda?', title_index: '2.14.', description: 'Sim, mas apenas até ao momento em que a mesma começa a ser preparada. Depois desta fase poderá, no entanto,
        proceder à devolução dos livros nos casos previstos.<br/>', status: :active, associated_faq_id: faq_2_1.id
continente_online.faqs << faq_2_14
online_note.faqs << faq_2_14

faq_2_15 = Faq.create(title: 'As escolas da Madeira e Açores têm direito a vouchers Mega?', title_index: '2.15.', description: 'Os arquipélagos têm gestão de educação autónoma e, por isso, não estão incluídos no programa MEGA.<br/><br/>', status: :active, associated_faq_id: faq_2_1.id
continente_online.faqs << faq_2_15
online_note.faqs << faq_2_15

faq_2_2 = Faq.create(title: 'Ainda não tenho voucher MEGA. É possível fazer a reserva?', title_index: '2.2.', description: 'No caso de ainda não ter voucher MEGA é possível selecionar e reservar os seus Livros, concluindo a sua encomenda
        apenas quando receber os seus vouchers. Efetue os seguintes passos:<ol class="ui list" role="list">
<li class="" role="listitem">Vá a “Ainda não tem os seus vouchers Mega? Faça já a sua pré-reserva” e clique em
            “Quero reservar os meus livros”.; </li>
<li class="" role="listitem">Preencha os campos com os seus dados e escolha os Livros que pretende reservar e
            clique em “Guardar Reserva”;</li>
<li class="" role="listitem">Introduza o seu contacto telefónico e proceda à validação do mesmo, através de um
            código 4 dígitos que irá receber por SMS;</li>
<li class="" role="listitem">Clique em “Guardar e Sair”, e esta será confirmada pelo envio de uma SMS e e-mail
            com o número da sua reserva.</li>
</ol><li class="" role="listitem">Vá a “Ainda não tem os seus vouchers Mega? Faça já a sua pré-reserva” e clique em
            “Quero reservar os meus livros”.; </li><li class="" role="listitem">Preencha os campos com os seus dados e escolha os Livros que pretende reservar e
            clique em “Guardar Reserva”;</li><li class="" role="listitem">Introduza o seu contacto telefónico e proceda à validação do mesmo, através de um
            código 4 dígitos que irá receber por SMS;</li><li class="" role="listitem">Clique em “Guardar e Sair”, e esta será confirmada pelo envio de uma SMS e e-mail
            com o número da sua reserva.</li><br/>', status: :active, associated_faq_id: faq_2_.id
continente_online.faqs << faq_2_2
online_note.faqs << faq_2_2

faq_2_3 = Faq.create(title: 'O que posso fazer se receber o voucher MEGA depois de já ter efetuado encomenda?', title_index: '2.3.', description: 'Caso tenha efetuado a sua encomenda e recebido o voucher MEGA posteriormente, pode adicioná-lo à encomenda, como
        faria normalmente. Regularizaremos a devolução do valor pago inicialmente no mesmo meio de pagamento utilizado
        na encomenda.<br/>', status: :active, associated_faq_id: faq_2_.id
continente_online.faqs << faq_2_3
online_note.faqs << faq_2_3

faq_2_4 = Faq.create(title: 'Efetuei a minha reserva. Como a posso concluí-la?', title_index: '2.4.', description: 'Quando já tiver os vouchers MEGA na sua posse, para concluir a sua encomenda, deverá efetuar os seguintes passos:
    <ol class="ui list" role="list">
<li class="" role="listitem">Clique no botão “Gerir Reserva”. Introduza o contacto telefónico, o número da sua
            encomenda (que recebeu no SMS e e-mail de confirmação) e clique em “Iniciar Sessão”;</li>
<li class="" role="listitem">Introduza os vouchers, preencha os campos sobre o Encapamento e escolha o método de
            entrega e de pagamento que pretende;</li>
<li class="" role="listitem">Para finalizar a sua encomenda, clique no botão “Concluir”.</li>
</ol><li class="" role="listitem">Clique no botão “Gerir Reserva”. Introduza o contacto telefónico, o número da sua
            encomenda (que recebeu no SMS e e-mail de confirmação) e clique em “Iniciar Sessão”;</li><li class="" role="listitem">Introduza os vouchers, preencha os campos sobre o Encapamento e escolha o método de
            entrega e de pagamento que pretende;</li><li class="" role="listitem">Para finalizar a sua encomenda, clique no botão “Concluir”.</li><br/>', status: :active, associated_faq_id: faq_2_.id
continente_online.faqs << faq_2_4
online_note.faqs << faq_2_4

faq_2_5 = Faq.create(title: 'Fiz a reserva, mas afinal não tenho direito a voucher, como devo proceder?', title_index: '2.5.', description: 'Caso pretenda concluir a sua encomenda sem voucher deverá clicar no botão “Gerir Reserva” e proceder da seguinte
        forma:<ol class="ui list" role="list">
<li class="" role="listitem">Introduza o contacto telefónico, o número da sua reserva e clique em “Iniciar
            Sessão”;</li>
<li class="" role="listitem">Preencha os campos sobre o Encapamento e insira o número do seu Cartão Continente
            ou Universo para usufruir dos descontos e vantagens associadas;</li>
<li class="" role="listitem">Escolha o método de entrega e de pagamento que pretende;</li>
<li class="" role="listitem">Para finalizar a sua encomenda, clique no botão “Concluir”.</li>
</ol><li class="" role="listitem">Introduza o contacto telefónico, o número da sua reserva e clique em “Iniciar
            Sessão”;</li><li class="" role="listitem">Preencha os campos sobre o Encapamento e insira o número do seu Cartão Continente
            ou Universo para usufruir dos descontos e vantagens associadas;</li><li class="" role="listitem">Escolha o método de entrega e de pagamento que pretende;</li><li class="" role="listitem">Para finalizar a sua encomenda, clique no botão “Concluir”.</li><br/>', status: :active, associated_faq_id: faq_2_.id
continente_online.faqs << faq_2_5
online_note.faqs << faq_2_5

faq_2_6 = Faq.create(title: 'Completei a minha reserva, mas não recebi confirmação?', title_index: '2.6.', description: 'Certifique-se que a reserva foi efetivamente concluída, clicando em ‘Gerir Reserva’. Verifique também se os dados
        introduzidos estão corretos. No caso de detetar algum erro pode retificar de imediato. Para qualquer questão
        adicional, p.f. contacte-nos através do formulário de contacto disponibilizado na página de contactos.<br/>', status: :active, associated_faq_id: faq_2_.id
continente_online.faqs << faq_2_6
online_note.faqs << faq_2_6

faq_2_7 = Faq.create(title: 'Como posso alterar a minha reserva?', title_index: '2.7.', description: 'Para alterar a sua reserva deverá clicar no botão “Gerir Reserva”. Introduza o contacto telefónico, o número da
        sua encomenda (que recebeu no SMS e e-mail de confirmação) e clique em “Iniciar Sessão” para ter acesso a todos
        os dados.<br/>', status: :active, associated_faq_id: faq_2_.id
continente_online.faqs << faq_2_7
online_note.faqs << faq_2_7

faq_2_8 = Faq.create(title: 'Como posso consultar o estado da minha Reserva?', title_index: '2.8.', description: 'Para consultar o estado da sua reserva, proceda da seguinte forma:<ol class="ui list" role="list">
<li class="" role="listitem">Clique em ‘Gerir Reserva’;</li>
<li class="" role="listitem">Consulte o estado da sua Reserva.</li>
</ol><li class="" role="listitem">Clique em ‘Gerir Reserva’;</li><li class="" role="listitem">Consulte o estado da sua Reserva.</li><p>Como alternativa, pode clicar no botão “Acompanhar Reserva” que se encontra no email de confirmação da encomenda
        que lhe foi enviado. </p><p><strong>ATENÇÃO: </strong>o processamento do seu pedido só poderá ser iniciado após ter concluído o pagamento (no
        método escolhido por si). No caso de uma reserva com vouchers MEGA, deverá garantir que os números dos vouchers
        estão devidamente associados.</p><strong>ATENÇÃO: </strong><br/>', status: :active, associated_faq_id: faq_2_.id
continente_online.faqs << faq_2_8
online_note.faqs << faq_2_8

faq_continente_online_2_9 = Faq.create(title: 'Que produtos posso comprar?', title_index: '2.9.', description: 'Pode comprar os Livros Escolares (Manuais, Cadernos de Atividades, Dicionários e Gramáticas) válidos para este
        ano letivo. Poderá adicionalmente adquirir o restante material escolar em <a href="https://www.continente.pt" rel="noreferrer" target="_blank">continente.pt</a><a href="https://www.continente.pt" rel="noreferrer" target="_blank">continente.pt</a><br/>', status: :active, associated_faq_id: faq_2_.id
continente_online.faqs << faq_continente_online_2_9

faq_online_note_2_9 = Faq.create(title: 'Que produtos posso comprar?', title_index: '2.9.', description: 'Pode comprar os Livros Escolares (Manuais, Cadernos de Atividades, Dicionários e Gramáticas) válidos para este
        ano letivo. Poderá adicionalmente adquirir o restante material escolar em <a href="https://www.noteonline.pt" rel="noreferrer" target="_blank">noteonline.pt</a><a href="https://www.noteonline.pt" rel="noreferrer" target="_blank">noteonline.pt</a><br/>', status: :active, associated_faq_id: faq_2_.id
online_note.faqs << faq_online_note_2_9

faq_3 = Faq.create(title: 'Pagamento', title_index: '3.', description: '', status: :activecontinente_online.faqs << faq_3
online_note.faqs << faq_3

faq_3_1 = Faq.create(title: 'Como se processa o pagamento e confirmação?', title_index: '3.1.', description: 'Após ter escolhido os Livros Escolares que pretende encomendar, proceda da seguinte forma:<ol class="ui list" role="list">
<li class="" role="listitem">Clique no botão “Comprar”;</li>
<li class="" role="listitem">No primeiro passo do checkout comece por selecionar a morada de entrega em
            “Detalhes de Entrega”; <ol class="">
<li class="" role="listitem">Caso pretenda receber a sua encomenda no seu domicílio, poderá fazê-lo
                    selecionando “Entrega ao Domicílio”;</li>
<li class="" role="listitem">Se pretender levantar a sua encomenda na loja, clique em “Entrega em loja”
                    e selecione a loja pretendida;</li>
</ol>
</li>
<li class="" role="listitem">Após escolher o método de entrega, valide o seu contacto telefónico através de um
            código de 4 dígitos que irá receber via SMS; </li>
<li class="" role="listitem">Para continuar, clique em “Escolher Método de Pagamento” e selecione o método que
            pretender;</li>
<li class="" role="listitem">Finalize a sua encomenda clicando em “Pagar” e confirmando o pagamento.</li>
</ol><li class="" role="listitem">Clique no botão “Comprar”;</li><li class="" role="listitem">No primeiro passo do checkout comece por selecionar a morada de entrega em
            “Detalhes de Entrega”; <ol class="">
<li class="" role="listitem">Caso pretenda receber a sua encomenda no seu domicílio, poderá fazê-lo
                    selecionando “Entrega ao Domicílio”;</li>
<li class="" role="listitem">Se pretender levantar a sua encomenda na loja, clique em “Entrega em loja”
                    e selecione a loja pretendida;</li>
</ol>
</li><ol class="">
<li class="" role="listitem">Caso pretenda receber a sua encomenda no seu domicílio, poderá fazê-lo
                    selecionando “Entrega ao Domicílio”;</li>
<li class="" role="listitem">Se pretender levantar a sua encomenda na loja, clique em “Entrega em loja”
                    e selecione a loja pretendida;</li>
</ol><li class="" role="listitem">Caso pretenda receber a sua encomenda no seu domicílio, poderá fazê-lo
                    selecionando “Entrega ao Domicílio”;</li><li class="" role="listitem">Se pretender levantar a sua encomenda na loja, clique em “Entrega em loja”
                    e selecione a loja pretendida;</li><li class="" role="listitem">Após escolher o método de entrega, valide o seu contacto telefónico através de um
            código de 4 dígitos que irá receber via SMS; </li><li class="" role="listitem">Para continuar, clique em “Escolher Método de Pagamento” e selecione o método que
            pretender;</li><li class="" role="listitem">Finalize a sua encomenda clicando em “Pagar” e confirmando o pagamento.</li><br/>', status: :active, associated_faq_id: faq_3_.id
continente_online.faqs << faq_3_1
online_note.faqs << faq_3_1

faq_3_2 = Faq.create(title: 'O valor apresentado é o valor que vou pagar?', title_index: '3.2.', description: 'Pagará sempre o valor que visualizar no site:<ul class="list">
<li class="content">No caso de ter direito a vouchers MEGA, esse valor será descontado quando estes forem
            adicionados;</li>
<li class="content">A esse valor, será acrescentado o custo de Encapamento, caso opte por este serviço;</li>
<li class="content">A esse valor será também acrescentado o custo de portes de envio, se a opção selecionada for
            a entrega ao domicílio (quando aplicável).</li>
</ul><li class="content">No caso de ter direito a vouchers MEGA, esse valor será descontado quando estes forem
            adicionados;</li><li class="content">A esse valor, será acrescentado o custo de Encapamento, caso opte por este serviço;</li><li class="content">A esse valor será também acrescentado o custo de portes de envio, se a opção selecionada for
            a entrega ao domicílio (quando aplicável).</li><br/>', status: :active, associated_faq_id: faq_3_.id
continente_online.faqs << faq_3_2
online_note.faqs << faq_3_2

faq_continente_online_3_3 = Faq.create(title: 'Tenho de pagar na totalidade ou posso pagar parcialmente?', title_index: '3.3.', description: 'O valor será cobrado na totalidade para todos os métodos de pagamento ou métodos de entrega.<br/>', status: :active, associated_faq_id: faq_3_.id
continente_online.faqs << faq_continente_online_3_3

faq_online_note_3_3 = Faq.create(title: 'Tenho de pagar na totalidade ou posso pagar parcialmente?', title_index: '3.3.', description: 'O valor será cobrado na totalidade para todos os métodos de pagamento ou métodos de entrega.<p>Excecionalmente, caso encomende os seus Livros Escolares ao balcão de uma loja Note!, poderá optar por pagar a
        mesma contra entrega, estando, no entanto, sujeito a uma caução de 15€ por encomenda (a deduzir ao valor a
        pagar).</p><p>O valor do sinal será devolvido no levantamento dos livros escolares, mediante a apresentação do talão de
        pagamento do serviço.</p><p>Caso não apresente o talão, não será possível efetuar a devolução.</p><br/>', status: :active, associated_faq_id: faq_3_.id
online_note.faqs << faq_online_note_3_3

faq_3_4 = Faq.create(title: 'Como posso alterar o método de pagamento?', title_index: '3.4.', description: 'Pode alterar imediatamente após ter escolhido o método de pagamento. Basta selecionar “Cancelar” e escolher o
        novo método de pagamento que pretende.<br/>', status: :active, associated_faq_id: faq_3_.id
continente_online.faqs << faq_3_4
online_note.faqs << faq_3_4

faq_continente_online_3_5 = Faq.create(title: 'Que meios de pagamento posso utilizar?', title_index: '3.5.', description: 'Tem à sua disposição os seguintes meios de pagamento:<ul class="list">
<li class="content">Vouchers Mega;</li>
<li class="content">Cartão de Crédito/Débito;</li>
<li class="content">MBWay;</li>
<li class="content">Referência MB.</li>
</ul><li class="content">Vouchers Mega;</li><li class="content">Cartão de Crédito/Débito;</li><li class="content">MBWay;</li><li class="content">Referência MB.</li><br/>', status: :active, associated_faq_id: faq_3_.id
continente_online.faqs << faq_continente_online_3_5

faq_online_note_3_5 = Faq.create(title: 'Que meios de pagamento posso utilizar?', title_index: '3.5.', description: 'Tem à sua disposição os seguintes meios de pagamento:<ul class="list">
<li class="content">Vouchers Mega;</li>
<li class="content">Cartão de Crédito/Débito;</li>
<li class="content">MBWay;</li>
<li class="content">Referência MB.</li>
</ul><li class="content">Vouchers Mega;</li><li class="content">Cartão de Crédito/Débito;</li><li class="content">MBWay;</li><li class="content">Referência MB.</li><div class="u-mt-1">
<p>Adicionalmente, e exclusivamente em loja, poderá utilizar:</p>
<ul class="list">
<li class="content">Cartão Dá;</li>
<li class="content">Ticket Ensino;</li>
<li class="content">Numerário.</li>
</ul>
</div><p>Adicionalmente, e exclusivamente em loja, poderá utilizar:</p><ul class="list">
<li class="content">Cartão Dá;</li>
<li class="content">Ticket Ensino;</li>
<li class="content">Numerário.</li>
</ul><li class="content">Cartão Dá;</li><li class="content">Ticket Ensino;</li><li class="content">Numerário.</li><p>Excecionalmente, caso encomende os seus Livros Escolares ao balcão de uma loja Note!, poderá optar por pagar a
        mesma contra entrega, estando, no entanto, sujeito a uma caução de 10€ por encomenda (a deduzir ao valor a
        pagar). </p><br/>', status: :active, associated_faq_id: faq_3_.id
online_note.faqs << faq_online_note_3_5

faq_3_6 = Faq.create(title: 'Como é faturada a minha encomenda?', title_index: '3.6.', description: 'A sua encomenda será sempre faturada por aluno. Assegure que coloca sempre o NIF do(s) aluno(s) para posterior
        utilização do documento.<p>Sempre que aplicável, a faturação será dividida em mais do que uma fatura:</p><ul class="list">
<li class="content">uma fatura referente à compra dos Livros Escolares;</li>
<li class="content">uma fatura referente aos portes de envio e custos de Encapamento.</li>
</ul><li class="content">uma fatura referente à compra dos Livros Escolares;</li><li class="content">uma fatura referente aos portes de envio e custos de Encapamento.</li><p>Terá sempre a possibilidade de consultar as faturas no site. Poderá ainda solicitar uma segunda via das suas
        faturas de Livros Escolares, a qualquer momento.</p><br/><br/>', status: :active, associated_faq_id: faq_3_.id
continente_online.faqs << faq_3_6
online_note.faqs << faq_3_6

faq_4 = Faq.create(title: 'Entregas', title_index: '4.', description: '', status: :activecontinente_online.faqs << faq_4
online_note.faqs << faq_4

faq_4_1 = Faq.create(title: 'Entregas em loja', title_index: '4.1.', description: '<br/>', status: :active, associated_faq_id: faq_4_.id
continente_online.faqs << faq_4_1
online_note.faqs << faq_4_1

faq_4_1_1 = Faq.create(title: 'Como posso seguir a minha encomenda?', title_index: '4.1.1.', description: 'Pode clicar no botão “Acompanhar Reserva” que se encontra no email de confirmação da encomenda que lhe foi
        enviado.<p><strong>NOTA:</strong> Após SMS/E-mail de disponibilização deverá proceder ao seu levantamento com a maior
        brevidade possível (na SMS/E-mail receberá informação sobre a data limite para o fazer).</p><strong>NOTA:</strong><br/>', status: :active, associated_faq_id: faq_4_1_.id
continente_online.faqs << faq_4_1_1
online_note.faqs << faq_4_1_1

faq_4_1_2 = Faq.create(title: 'Posso alterar o método de entrega em loja para entrega ao domicílio?', title_index: '4.1.2.', description: 'Não. Após selecionar o método de entrega não poderá proceder à sua alteração.<br/>', status: :active, associated_faq_id: faq_4_1_.id
continente_online.faqs << faq_4_1_2
online_note.faqs << faq_4_1_2

faq_4_1_3 = Faq.create(title: 'Posso alterar a loja onde vai ser entregue a encomenda?', title_index: '4.1.3.', description: 'Para alterar a loja de entrega deverá clicar no botão “Gerir Reserva” e efetuar os seguintes passos:<ol class="ui list" role="list">
<li class="" role="listitem">Introduza o contacto telefónico;</li>
<li class="" role="listitem">Introduza o número da sua encomenda;</li>
<li class="" role="listitem">Clique em “Iniciar Sessão”.</li>
</ol><li class="" role="listitem">Introduza o contacto telefónico;</li><li class="" role="listitem">Introduza o número da sua encomenda;</li><li class="" role="listitem">Clique em “Iniciar Sessão”.</li><p>Será redirecionado para uma página onde consta toda a informação sobre a sua encomenda e onde poderá alterar a
        loja de entrega, até a encomenda ser preparada. </p><p><strong>NOTA: </strong>Após o início da preparação da encomenda já não será possível proceder a alterações à
        mesma.</p><strong>NOTA: </strong><br/>', status: :active, associated_faq_id: faq_4_1_.id
continente_online.faqs << faq_4_1_3
online_note.faqs << faq_4_1_3

faq_4_2 = Faq.create(title: 'Entregas ao domicílio', title_index: '4.2.', description: '<br/>', status: :active, associated_faq_id: faq_4_.id
continente_online.faqs << faq_4_2
online_note.faqs << faq_4_2

faq_4_2_1 = Faq.create(title: 'Como posso seguir a minha encomenda depois de ser enviada?', title_index: '4.2.1.', description: 'Para seguir sua encomenda deverá clicar no link de rastreamento, que irá receber no seu e-mail por parte da
        transportadora (logo que a mesma seja enviada). Será redirecionado para uma página da transportadora onde consta
        toda a informação de seguimento da sua encomenda.<br/>', status: :active, associated_faq_id: faq_4_2_.id
continente_online.faqs << faq_4_2_1
online_note.faqs << faq_4_2_1

faq_4_2_2 = Faq.create(title: 'Como posso alterar a morada de entrega?', title_index: '4.2.2.', description: 'Para alterar a morada de entrega deverá clicar no botão “Gerir Reserva” e efetuar os seguintes passos:<ol class="ui list" role="list">
<li class="" role="listitem">Introduza o contacto telefónico;</li>
<li class="" role="listitem">Introduza o número da sua encomenda; </li>
<li class="" role="listitem">Clique em “Iniciar Sessão”.</li>
</ol><li class="" role="listitem">Introduza o contacto telefónico;</li><li class="" role="listitem">Introduza o número da sua encomenda; </li><li class="" role="listitem">Clique em “Iniciar Sessão”.</li><p>Será redirecionado para uma página onde consta toda a informação sobre a sua encomenda e onde poderá alterar a
        morada de entrega, até a encomenda ser preparada.</p><p><strong>NOTA:</strong> Após o início da preparação da encomenda já não será possível proceder a alterações à
        mesma.</p><strong>NOTA:</strong><br/>', status: :active, associated_faq_id: faq_4_2_.id
continente_online.faqs << faq_4_2_2
online_note.faqs << faq_4_2_2

faq_4_2_3 = Faq.create(title: 'Posso alterar o método de entrega de domicílio para loja?', title_index: '4.2.3.', description: 'Não. Após selecionar o método de entrega não poderá proceder à sua alteração.<br/>', status: :active, associated_faq_id: faq_4_2_.id
continente_online.faqs << faq_4_2_3
online_note.faqs << faq_4_2_3

faq_4_2_4 = Faq.create(title: 'Quais são as áreas de entrega?', title_index: '4.2.4.', description: 'Efetuamos entregas em Portugal. Ao adicionar a sua morada, conseguirá confirmar a respetiva disponibilidade.<br/><br/>', status: :active, associated_faq_id: faq_4_2_.id
continente_online.faqs << faq_4_2_4
online_note.faqs << faq_4_2_4

faq_5 = Faq.create(title: 'Devoluções e Substituições', title_index: '5.', description: '', status: :activecontinente_online.faqs << faq_5
online_note.faqs << faq_5

faq_5_1 = Faq.create(title: 'É possível devolver uma encomenda?', title_index: '5.1.', description: 'É possível fazer a devolução da Encomenda nos seguintes casos:<ol class="ui list" role="list">
<li class="" role="listitem">Artigos danificados;</li>
<li class="" role="listitem">Artigos não correspondentes à encomenda;</li>
<li class="" role="listitem">Mudança de escola/manual adotado.</li>
</ol><li class="" role="listitem">Artigos danificados;</li><li class="" role="listitem">Artigos não correspondentes à encomenda;</li><li class="" role="listitem">Mudança de escola/manual adotado.</li><br/>', status: :active, associated_faq_id: faq_5_.id
continente_online.faqs << faq_5_1
online_note.faqs << faq_5_1

faq_continente_online_5_2 = Faq.create(title: 'Como posso devolver?', title_index: '5.2.', description: 'Para devolver algum produto que tenha adquirido, basta dirigir-se a uma loja Continente. Deve levar consigo os
        produtos que deseja devolver, se possível, devidamente embalados, bem como uma cópia da fatura.<br/>', status: :active, associated_faq_id: faq_5_.id
continente_online.faqs << faq_continente_online_5_2

faq_online_note_5_2 = Faq.create(title: 'Como posso devolver?', title_index: '5.2.', description: 'Para devolver algum produto que tenha adquirido, basta dirigir-se a uma loja Note!. Deve levar consigo os
        produtos que deseja devolver, se possível, devidamente embalados, bem como uma cópia da fatura.<br/>', status: :active, associated_faq_id: faq_5_.id
online_note.faqs << faq_online_note_5_2

faq_5_3 = Faq.create(title: 'Como se realiza a devolução do montante pago?', title_index: '5.3.', description: 'O montante é reembolsado no momento da devolução através do mesmo meio de pagamento com que efetuou a sua
        Encomenda. A data de disponibilização efetiva do reembolso poderá depender de entidades terceiras.<br/>', status: :active, associated_faq_id: faq_5_.id
continente_online.faqs << faq_5_3
online_note.faqs << faq_5_3

faq_5_4 = Faq.create(title: 'É necessário embalar os Livros Escolares a devolver?', title_index: '5.4.', description: 'Preferencialmente, os Livros Escolares devem ser devolvidos, embalados na caixa em que foram entregues. Contudo,
        como a devolução é obrigatoriamente realizada em loja, caso já não consiga entregar com a caixa de origem, basta
        garantir que os mesmos estão em bom estado. <br/><br/>', status: :active, associated_faq_id: faq_5_.id
continente_online.faqs << faq_5_4
online_note.faqs << faq_5_4

faq_6 = Faq.create(title: 'Encapamento', title_index: '6.', description: '', status: :activecontinente_online.faqs << faq_6
online_note.faqs << faq_6

faq_6_1 = Faq.create(title: 'Posso encomendar os Livros Escolares já encapados?', title_index: '6.1.', description: 'Sim, ao encomendar os seus Livros Escolares poderá recebê-los já encapados! Para isso terá de escolher o serviço
        de Encapamento durante a sua encomenda e selecionar os Livros que pretende encapar.<br/>', status: :active, associated_faq_id: faq_6_.id
continente_online.faqs << faq_6_1
online_note.faqs << faq_6_1

faq_6_2 = Faq.create(title: 'Como é feito o Encapamento?', title_index: '6.2.', description: 'Para Encapamento dos Livros Escolares são utilizadas bolsas de encapar com as seguintes caraterísticas:<ul class="list">
<li class="content">Fáceis de retirar para quem pretender dar ou devolver os Livros Escolares no final do ano
            letivo;</li>
<li class="content">Certificadas em termos de segurança para uso infantil;</li>
<li class="content">100% recicláveis;</li>
<li class="content">Inodoras;</li>
<li class="content">Resistentes à água e fáceis de higienizar;</li>
<li class="content">Não deixam bolhas de ar.</li>
</ul><li class="content">Fáceis de retirar para quem pretender dar ou devolver os Livros Escolares no final do ano
            letivo;</li><li class="content">Certificadas em termos de segurança para uso infantil;</li><li class="content">100% recicláveis;</li><li class="content">Inodoras;</li><li class="content">Resistentes à água e fáceis de higienizar;</li><li class="content">Não deixam bolhas de ar.</li><br/><br/><br/>', status: :active, associated_faq_id: faq_6_.id
continente_online.faqs << faq_6_2
online_note.faqs << faq_6_2

