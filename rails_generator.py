﻿import json
from utils import *
from faq import dict_to_faq_list

# Convert index to variable name
def index_to_variable(title_index, prefix='faq_'):
    return prefix + title_index.replace('.', '_')[:-1]

# Returns a string that creates a faq and adds it to the stores and associate it with its parent
def generate_faq(faq, got_parent=False):
    if len(faq.stores) == 1:
        faq_variable_name = index_to_variable(faq.title_index, prefix=f'faq_{faq.stores[0]}_')
    else:
        faq_variable_name = index_to_variable(faq.title_index)

    command = f"{faq_variable_name} = Faq.create(title: '{faq.title}', title_index: '{faq.title_index}', description: '{faq.description}', status: {faq.status}"

    if got_parent:
        parent_variable_name = index_to_variable(get_super_index(faq.title_index))
        command += f", associated_faq_id: {parent_variable_name}.id\n"

    for store in faq.stores:
        command += f"{store}.faqs << {faq_variable_name}\n"

    return command

# Generate a rails seed file from a json file
def generate_rails_seed(json_file):
    print(JSON_FILE_PATH + json_file)
    with open(JSON_FILE_PATH + json_file, encoding='UTF-8') as f:
        dict_list = json.load(f)

    faqs = dict_to_faq_list(dict_list)
    stores = set( [store for faq in faqs for store in faq.stores] )
    command = ""

    for store in stores:
        command += f"{store.replace('-', '_')} = Store.find_by(slug: '{store}')\n"

    for faq in faqs:
        faq.stores = [store.replace('-', '_') for store in faq.stores]
        has_parent = get_level(faq.title_index) > 1
        command += generate_faq(faq, has_parent) + "\n"

    with open(RUBY_FILE_PATH + "rails_seed.rb", 'w', encoding='UTF-8') as f:
        f.write(command)