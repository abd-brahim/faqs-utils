﻿from utils import faqStatusSymbols, FaqStatus
import json
import re

class Faq:
    def __init__(self, title, title_index, description, store, status = faqStatusSymbols[FaqStatus.PUBLISHED]):
        self.title = title
        self.title_index = title_index
        self.description = description
        self.status = status
        self.set_store(store)
        self.remove_newline()
        self.remove_first_paragraph()

    def set_store(self, store):
        if type(store) is str:
            self.stores = [store]
        else:
            self.stores = []
            self.stores.extend(store)

    def add_store(self, store):
        if store not in self.stores:
            self.stores.append(store)
    
    def add_stores(self, stores):
        for store in stores:
            self.add_store(store)

    def remove_newline(self):
        re.sub(r"\n(?=[^\n\t])", " ", self.title)
        re.sub(r"\n(?=[^\n\t])", " ", self.title_index)
    
    def remove_first_paragraph(self):
        if self.description.startswith("<p>"):
            self.description = self.description[3:].replace("</p>", "", 1)

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    def __str__(self):
        return f"Title: {self.title} \nTitle Index: {self.title_index} \nDescription: {self.description} \nStatus: {self.status}"
    
    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, Faq):
            return False
        return self.title_index == __o.title_index and self.title == __o.title and self.description == __o.description and self.status == __o.status
    
def dict_to_faq_list(dict_list):
    faq_list = []
    for faq_dict in dict_list:
        faq_list.append(Faq(faq_dict['title'], faq_dict['title_index'], faq_dict['description'], faq_dict['stores'], faq_dict['status']))
    return faq_list