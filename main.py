import os
import json
from utils import *
from faq import Faq
from rails_generator import generate_rails_seed

def join_store_faqs(stores):
    faqs_list = []

    for store in stores:
        with open(f"json/{store}.json", encoding='UTF-8') as f:
            faqs_list += json.load(f)

    faqs_list = [Faq(faq['title'], faq['title_index'], faq['description'], faq['stores']) for faq in faqs_list]
    faqs = []
    
    for faq in faqs_list:
        if faq not in faqs:
            faqs.append(faq)
        else:
            faqs[faqs.index(faq)].add_stores(faq.stores)
    
    # Sort the faqs by title_index
    faqs.sort(key=lambda faq: faq.title_index)

    # Write the faqs to a json file
    with open("json/faqs.json", 'w', encoding='UTF-8') as f:
        f.write('[\n')
        for faq in faqs:
            f.write(f"{faq.to_json()},\n")

        f.seek(f.tell() - 2, os.SEEK_SET)
        f.write('\n]')

def main():
    generate_rails_seed("faqs.json")

if __name__ == "__main__":
    main()