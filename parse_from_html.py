﻿from bs4 import BeautifulSoup
from utils import *

def read_from_html(store_name):
    with open(f"{HTML_FILE_PATH}{store_name}.html", "r", encoding='UTF-8') as f:
        soup = BeautifulSoup(f.read(), 'html.parser')

    faq_list = []
    index = ""
    title = ""
    description = ""

    for tag in soup.find_all():
        if is_h_tag(tag):
            if index != "":
                faq_list.append(Faq(title, index, description, store_name))
                description = ""
                
            index, title = tag.text.split(' ', 1)
            title = title.rstrip()
        else:
            description += str(tag)

    for faq in faq_list:
        if get_level(faq.title_index) == 1:
            faq.description = ""

    with open(f"{JSON_FILE_PATH}{store_name}.json", 'w', encoding='UTF-8') as f:
        f.write('[\n')
        for faq in faq_list:
            f.write(f"{faq.to_json()},")

        f.seek(f.tell() - 1, os.SEEK_SET)
        f.write(']')